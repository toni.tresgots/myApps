//
//  UIViewExt.swift
//  breakpoint
//
//  Created by Tony Tresgots on 17/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

extension UIView {
    
    func bindToKeyboard() {         // allow to move where we want / marche avec les boutons car équivalent UIView
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChange(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
    }
    
    @objc func keyboardWillChange(_ notification: NSNotification) {
        
        let duration = notification.userInfo![UIKeyboardAnimationDurationUserInfoKey] as! Double       // on reprend la durée du keyboard qui pull up pour l'attribuer aux UIView ( afin qu'ils pull up en meme temps )
        
        let curve = notification.userInfo![UIKeyboardAnimationCurveUserInfoKey] as! UInt  // curve --> animation on reprend celle du keyboard pour UIView aussi ( coordination avec la montée du clavier )
        
        let beginningFrame = (notification.userInfo![UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue     // on coordonne le début de la montée de la view avec celle du keyboard
        
        let endFrame = (notification.userInfo![UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue  // ainsi que la descente
        
        let deltaY = endFrame.origin.y - beginningFrame.origin.y       // nécéssaire pour move la view au bon endroit
        
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIViewKeyframeAnimationOptions(rawValue: curve), animations: {
            self.frame.origin.y += deltaY     // move la View au bon endroit par rapport au keyboard
        }, completion: nil)
    }
}











