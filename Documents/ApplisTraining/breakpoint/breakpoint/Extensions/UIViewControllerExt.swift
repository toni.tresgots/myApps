//
//  UIViewControllerExt.swift
//  breakpoint
//
//  Created by Tony Tresgots on 22/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

extension UIViewController {        // transition entre view controllers
    func presentDetail(_ viewControllerToPresent: UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush     // type de la transition
        transition.subtype = kCATransitionFromRight // direction
        self.view.window?.layer.add(transition, forKey: kCATransition)  // add the transition
        
        present(viewControllerToPresent, animated: false, completion: nil)
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush     // type de la transition
        transition.subtype = kCATransitionFromLeft // direction
        self.view.window?.layer.add(transition, forKey: kCATransition)  // add the transition
        
        dismiss(animated: false, completion: nil)
        
    }
}
