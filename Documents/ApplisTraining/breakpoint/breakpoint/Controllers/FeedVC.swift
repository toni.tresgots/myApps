//
//  FirstViewController.swift
//  breakpoint
//
//  Created by Tony Tresgots on 13/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

class FeedVC: UIViewController {

    var messageArray = [Message]()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DataService.instance.getAllFeedMessages { (returnedMessagesArray) in
            self.messageArray = returnedMessagesArray.reversed()    // nouveaux messages apparaissent at the top au lieu de at the bottom par default
            self.tableView.reloadData()
        }
    }

}


extension FeedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messageArray.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? FeedCell else { return UITableViewCell()}
        
        let image = UIImage(named: "defaultProfileImage")
        let message = messageArray[indexPath.row]
        
        DataService.instance.getUsername(forUID: message.senderId) { (returnedUsername) in   // returnedUsername --> valeur de la transformation de l'id en userName de la func
            cell.configureCell(profileImg: image!, email: returnedUsername, message: message.content)
        }
        return cell
    }
}




