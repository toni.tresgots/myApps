//
//  CreateGroupsVC.swift
//  breakpoint
//
//  Created by Tony Tresgots on 18/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import Firebase

class CreateGroupsVC: UIViewController {

    
    var emailArray = [String]()
    var chosenUserArray = [String]()
    
    
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var emailSearchTextField: UITextField!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupMembersLbl: UILabel!
    
    @IBAction func doneBtnPressed(_ sender: Any) {
        if titleTextField.text != "" && descriptionTextField.text != "" {
            DataService.instance.getIds(forUsernames: chosenUserArray, handler: { (idsArray) in
                var userIds = idsArray
                userIds.append((Auth.auth().currentUser?.uid)!)     // ajouter l'id du créateur du groupe dans le tableau des members
                
                DataService.instance.createGroup(withTitle: self.titleTextField.text!, andDescription: self.descriptionTextField.text!, forUserIds: userIds, handler: { (groupCreated) in
                    if groupCreated {
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        print("Error group not created")
                    }
                })
            })
        }
    }
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        emailSearchTextField.delegate = self
        emailSearchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        doneBtn.isHidden = true
    }

    @objc func textFieldDidChange() {
        if emailSearchTextField.text == "" {
            emailArray = []
            tableView.reloadData()
        } else {
            DataService.instance.getEmail(forSearchQuery: emailSearchTextField.text!, handler: { (returnedEmailArray) in
                self.emailArray = returnedEmailArray    // on stock les resultat de FB dans emailArray
                self.tableView.reloadData()
            })  // query --> ce que tape l'user / des qu'il change une lettre --> reloadData grace au editingChanged du viewDidLoad()
        }
    }
    
}


extension CreateGroupsVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emailArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "userCell") as? UserCell else { return UITableViewCell()}
        
        let profileImg = UIImage(named: "defaultProfileImage")
        
        if chosenUserArray.contains(emailArray[indexPath.row]) {
            cell.configureCell(profileImage: profileImg!, email: emailArray[indexPath.row], isSelected: true)
        } else {
            cell.configureCell(profileImage: profileImg!, email: emailArray[indexPath.row], isSelected: false)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? UserCell else { return }
        if !chosenUserArray.contains(cell.userEmailLbl.text!) {     // si la cell n'ai pas encore affichée dans le chosenUserArray alors on l'append
            chosenUserArray.append(cell.userEmailLbl.text!)
            groupMembersLbl.text = chosenUserArray.joined(separator: ", ")      // séparer les différents user emails selectionné dans la cell
            doneBtn.isHidden = false
        } else {
            chosenUserArray = chosenUserArray.filter({ $0 != cell.userEmailLbl.text! })     // update the array with  only cells who aren't in the array yet
            if chosenUserArray.count > 0 {      // si nous avons plusieurs chosen email alors
                groupMembersLbl.text = chosenUserArray.joined(separator: ", ")
            } else {    // si 0 chosen emails alors
                groupMembersLbl.text = "Add people to your group"
                doneBtn.isHidden = true
            }
        }
    }
}


extension CreateGroupsVC: UITextFieldDelegate {
    
    
    
}







