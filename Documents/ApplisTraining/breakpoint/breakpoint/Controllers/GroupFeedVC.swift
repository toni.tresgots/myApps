//
//  GroupFeedVC.swift
//  breakpoint
//
//  Created by Tony Tresgots on 21/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import Firebase

class GroupFeedVC: UIViewController {
    
    var group: Group?
    var groupMessages = [Message]()

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var groupTitleLbl: UILabel!
    @IBOutlet weak var sendBtnView: UIView!
    @IBOutlet weak var messageTextField: InsetTextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var membersLbl: UILabel!
    
    @IBAction func closeBtnPressed(_ sender: Any) {
        dismissDetail()
    }
    
    @IBAction func sendBtnPressed(_ sender: Any) {
        if messageTextField.text != "" {
            messageTextField.isEnabled = false
            sendBtn.isEnabled = false
            DataService.instance.uploadPost(withMessage: messageTextField.text!, forUID: (Auth.auth().currentUser?.uid)!, withGroupKey: group?.key, sendComplete: { (complete) in
                if complete {
                    self.messageTextField.text = ""      // une fois message envoyer on remet text vide
                    self.messageTextField.isEnabled = true
                    self.sendBtn.isEnabled = true
                }
            })
            
        }
    }
    
    func initData(forGroup group: Group) {
        self.group = group          // accès aux données de la class Group ( .groupTitle etc )
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        groupTitleLbl.text = group?.groupTitle      // affiche titre et membres ( id des membres )
        
        DataService.instance.getEmails(group: group!) { (returnedEmails) in // besoin de cette func pour transformer les ids users en emails
            self.membersLbl.text = returnedEmails.joined(separator: ", ")   // returnedEmail -> nouveau tableau contenant que les emails
        }
        
        DataService.instance.REF_GROUPS.observe(.value) { (snapshot) in     // if anything change ( new post, delete ... ) update tableView
            DataService.instance.getAllMessagesFor(desiredGroup: self.group!, handler: { (returnedGroupMessages) in
                self.groupMessages = returnedGroupMessages
                self.tableView.reloadData()
                
                if self.groupMessages.count > 0 {
                    self.tableView.scrollToRow(at: IndexPath(row: self.groupMessages.count - 1, section: 0), at: .none, animated: true)     // scroll tableView to the top
                }
            })
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        sendBtnView.bindToKeyboard()
    }
}

extension GroupFeedVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupMessages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "groupFeedCell", for: indexPath) as? GroupFeedCell else { return UITableViewCell() }
        
        let message = groupMessages[indexPath.row]
        
        DataService.instance.getUsername(forUID: message.senderId) { (email) in
            cell.configureCell(profileImg: UIImage(named: "defaultProfileImage")!, email: email, content: message.content)

        }
        return cell
    }
}
