//
//  MeVC.swift
//  breakpoint
//
//  Created by Tony Tresgots on 16/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import Firebase

class MeVC: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    @IBAction func logOutButtonPressed(_ sender: UIButton) {
        let logoutPopUp = UIAlertController(title: "Log Out", message: "Are you sure you want to log out ?", preferredStyle: .actionSheet)
        
        let logoutAction = UIAlertAction(title: "Log Out", style: .destructive) { (buttonTapped) in
            do {
                try Auth.auth().signOut()   // log out user
                let authVC = self.storyboard?.instantiateViewController(withIdentifier: "AuthVC") as? AuthVC
                self.present(authVC!, animated: true, completion: nil)
            } catch {
                print(error)
            }
        }
        logoutPopUp.addAction(logoutAction)
        present(logoutPopUp, animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        emailLabel.text = Auth.auth().currentUser?.email       // affiche l'email du user enregistré

    }


}
