//
//  DataServices.swift
//  breakpoint
//
//  Created by Tony Tresgots on 13/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.


import Foundation
import Firebase

let DB_BASE = Database.database().reference()          // allow access to the database / c'est un URL de la database


class DataService {
    static let instance = DataService()        // to make the class accessible everywhere

    // store Firebase references / private --> data hiding, accessible only dans le DataServices
    private let _REF_BASE = DB_BASE
    private let _REF_USERS = DB_BASE.child("users")         // Creation d'un folder "users" in Firebase
    private let _REF_GROUPS = DB_BASE.child("groups")       // Creation folder "groups"
    private let _REF_FEED = DB_BASE.child("feed")           // Creation folder "feed"

    var REF_BASE: DatabaseReference {
        return _REF_BASE            // simple protection de données / data hiding
    }

    var REF_USERS: DatabaseReference {
        return _REF_USERS
    }

    var REF_GROUPS: DatabaseReference {
        return _REF_GROUPS
    }

    var REF_FEED: DatabaseReference {
        return _REF_FEED
    }


    func createDBUser(uid: String, userData: Dictionary<String, Any>) {     // userData --> dictionnaire de plusieurs élément ( email etc )
        REF_USERS.child(uid).updateChildValues(userData)  // Push values to Firebase / Create a firebase user
    }

    func getUsername(forUID uid: String, handler: @escaping (_ username: String) -> ()) {      // recupérer le username pour l'afficher à la place du iD
        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in
            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for user in userSnapshot {  // boucle for car cherche le user correspondant dans les datas
                if user.key == uid {        // si l'id de Firebase correspond avec celui que l'user a saisi ...
                    handler(user.childSnapshot(forPath: "email").value as! String)  // recupere la value du key email de l'id correspondant
                }
            }
            
        }
        
    }
    
    func uploadPost(withMessage message: String, forUID uid: String, withGroupKey groupKey: String?, sendComplete: @escaping (_ status: Bool) -> ()) {
        if groupKey != nil {        // signifie si le message est envoyer dans un groupe, alors on envoi au groupe ref
            REF_GROUPS.child(groupKey!).child("messages").childByAutoId().updateChildValues(["content": message, "senderId": uid])  // upload 1- le groupe correspondant, 2- folder FB "messages" crée, 3- créer un id random pour chaques post, 4- créer les attributs du folder messages
            sendComplete(true)
        } else {        // si pas de groupe, alors envoyé sur globale feed
            REF_FEED.childByAutoId().updateChildValues(["content": message, "senderId": uid])           // childByAutoId crée un id pour chaque message automatique
            sendComplete(true)
        }
    }
    
    func getAllFeedMessages(handler: @escaping (_ messages: [Message]) -> ()) {
        var messageArray = [Message]()
        REF_FEED.observeSingleEvent(of: .value) { (feedMessageSnapshot) in   // observe everything in the Feed and download values in the feedMessageSnapshot array
            guard let feedMessageSnapshot = feedMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for message in feedMessageSnapshot {
                let content = message.childSnapshot(forPath: "content").value as! String        // récupere la value du key "content" dans Firebase afin de pouvoir l'afficher
                let senderId = message.childSnapshot(forPath: "senderId").value as! String  // recupere la valeur du key "senderId" dans Firebase
                let message = Message(content: content, senderId: senderId)     // message from Firebase retrieved
                messageArray.append(message)
            }
            handler(messageArray)
        }
    }
    
    func getAllMessagesFor(desiredGroup: Group, handler: @escaping(_ messagesArray: [Message]) -> ()) {
        var groupMessageArray = [Message]()
        
        REF_GROUPS.child(desiredGroup.key).child("messages").observeSingleEvent(of: .value) { (groupMessageSnapshot) in
            guard let groupMessageSnapshot = groupMessageSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for groupMessage in groupMessageSnapshot {
                let content = groupMessage.childSnapshot(forPath: "content").value as! String
                let senderId = groupMessage.childSnapshot(forPath: "senderId").value as! String
                let groupMessage = Message(content: content, senderId: senderId)
                groupMessageArray.append(groupMessage)
            }
            handler(groupMessageArray)
        }
    }
    
    func getEmail(forSearchQuery query: String, handler: @escaping (_ emailArray: [String]) -> ()) {
        var emailArray = [String]() // nouveau tableaux qui va prendre recuperer les emails du array de firebase
        
        REF_USERS.observe(.value) { (userSnapshot) in   // permet de rechercher une valeur dans le "user" de FB
            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for user in userSnapshot {
                let email = user.childSnapshot(forPath: "email").value as! String
                
                if email.contains(query) == true && email != Auth.auth().currentUser?.email {      // si un email dans Firebase == email saisie par le user && email != de celui qui crée le groupe
                    emailArray.append(email)        // alors on l'ajoute au nouveau tableau crée
                }
            }
            handler(emailArray)
        }
    }
    
    func getIds(forUsernames usernames: [String], handler: @escaping(_ uidArray: [String]) -> ()) {
        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in    // pull up all users from firebase
            var idArray = [String]()
            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
            // if we got values in REF_USERS alors ...
            // Need to compare array of users from Firebase and the array we pass in
            for user in userSnapshot {
                
                let email = user.childSnapshot(forPath: "email").value as! String
                if usernames.contains(email) {
                    idArray.append(user.key)    // remplie le nouveau tableau avec les keys ( qui sont les uid )
                }
            }
            handler(idArray)
        }
    }
    
    func getEmails(group: Group, handler: @escaping (_ emailArray: [String]) -> ()) {   // transforme les id en emails
        var emailArray = [String]()     // array qui va stocker les email
        
        REF_USERS.observeSingleEvent(of: .value) { (userSnapshot) in
            guard let userSnapshot = userSnapshot.children.allObjects as? [DataSnapshot] else { return }
            for user in userSnapshot {
                if group.members.contains(user.key) {       // pour tout les user du groupe qui ont un key
                    let email = user.childSnapshot(forPath: "email").value as! String
                    emailArray.append(email)        // on ajoute leur email dans le nouveau tableau
                }
            }
            handler(emailArray)
        }
        
    }
    
    func createGroup(withTitle title: String, andDescription description: String, forUserIds ids: [String], handler: @escaping (_ groupCreated: Bool) -> ()) {
        REF_GROUPS.childByAutoId().updateChildValues(["title": title, "description": description, "members": ids])  // créer des folder dans le folder Groups
        handler(true)
    }
    
    func getAllGroups(handler: @escaping(_ groupsArray : [Group]) -> ()) {      // Download all the groups inside the app
        
        var groupArray = [Group]()
        
        REF_GROUPS.observeSingleEvent(of: .value) { (groupSnapshot) in
            guard let groupSnapshot = groupSnapshot.children.allObjects as? [DataSnapshot] else { return }
            
            for group in groupSnapshot {
                let membersArray = group.childSnapshot(forPath: "members").value as! [String]
                if membersArray.contains((Auth.auth().currentUser?.uid)!) {
                    let title = group.childSnapshot(forPath: "title").value as! String
                    let description = group.childSnapshot(forPath: "description").value as! String
                    
                    let group = Group(groupTitle: title, groupDesc: description, key: group.key, memberCount: membersArray.count, members: membersArray)
                    groupArray.append(group)
                    
                }
            }
            handler(groupArray)
        }
        
    }
}






















