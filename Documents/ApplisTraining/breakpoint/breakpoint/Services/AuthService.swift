
import Firebase
import Foundation

class AuthService {
    static let instance = AuthService()             // static --> allow the instance to be accessible during the entire cycle of the app running
    
    func registerUser(withEmail email: String, andPassword password: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error?) -> () ) {        // si nous avons bien un email et un password alors status --> true /  si une error dans l'email ou le password --> error
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in   // fonction de firebase - create an user
            
            guard let user = user else {
                userCreationComplete(false, error)      // if user n'est pas égal a user
                return
            }
            // if user = user
            let userData = ["provider": user.providerID, "email": user.email]   // provider -> firebase func ( d'ou viens le user ex : facebook, google etc )
            DataService.instance.createDBUser(uid: user.uid, userData: userData)    // tout les user ont un uid deja attribué automatiquement ( pas besoin d'en créer )
            userCreationComplete(true, nil)
            
        }
        
    }
    
    func loginUser(withEmail email: String, andPassword password: String, loginComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                loginComplete(false, error)
                return
            }
            loginComplete(true, nil)
        }
    }
}
