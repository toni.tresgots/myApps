//
//  UserCell.swift
//  breakpoint
//
//  Created by Tony Tresgots on 18/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

class UserCell: UITableViewCell {
    
    var showing = false     // sera utilisé pour faire apparaitre la checkMark sur plusieurs cells en meme temps
    
    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var userEmailLbl: UILabel!
    @IBOutlet weak var checkMark: UIImageView!
    
    func configureCell(profileImage image: UIImage, email: String, isSelected: Bool) {
        self.profileImg.image = image
        self.userEmailLbl.text = email
        if isSelected {
            self.checkMark.isHidden = false
        } else {
            self.checkMark.isHidden = true
        }
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {           // si cell is selected alors ...
            if showing == false {
                checkMark.isHidden = false
                showing = true
            } else {
                checkMark.isHidden = true
                showing = false
            }
        }
        
    }
}
