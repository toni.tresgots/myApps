//
//  ShadowView.swift
//  breakpoint
//
//  Created by Tony Tresgots on 14/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

class ShadowView: UIView {          // gonna set up a shadow animation on AuthVC
    
    override func awakeFromNib() {
        self.layer.shadowOpacity = 0.75         // shadow opacity
        self.layer.shadowRadius = 5
        self.layer.shadowColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        super.awakeFromNib()
    }
    
    
}
