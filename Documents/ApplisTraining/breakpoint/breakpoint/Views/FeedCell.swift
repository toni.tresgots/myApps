//
//  FeedCell.swift
//  breakpoint
//
//  Created by Tony Tresgots on 18/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

class FeedCell: UITableViewCell {

    @IBOutlet weak var profileImg: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var messageContent: UILabel!
    
    func configureCell(profileImg: UIImage, email: String, message: String) {
        
        self.profileImg.image = profileImg
        self.emailLbl.text = email
        self.messageContent.text = message
    }
    
}
