//
//  DroppablePin.swift
//  pixel-city
//
//  Created by Tony Tresgots on 03/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class DroppablePin: NSObject, MKAnnotation {
    dynamic var coordinate : CLLocationCoordinate2D
    var identifier: String
    
    init(coordinate: CLLocationCoordinate2D, identifier: String){
        self.coordinate = coordinate
        self.identifier = identifier
        super.init()
    }
}
