//
//  PhotoCell.swift
//  pixel-city
//
//  Created by Tony Tresgots on 04/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {                   // needed for custom collectionView Cell
        fatalError("init(coder:) has not been implemented")
    }
}
