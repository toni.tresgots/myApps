//
//  Constants.swift
//  pixel-city
//
//  Created by Tony Tresgots on 04/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import Foundation

let apiKey = "415a410a910a97433a396be6fd7bd64a"



func flickrURL(forApiKey key: String, withAnnotation annotation: DroppablePin, NumberOfPhotos number: Int) -> String {
    let url =  "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=\(apiKey)&lat=\(annotation.coordinate.latitude)&lon=\(annotation.coordinate.longitude)&radius=1&radius_units=km&per_page=\(number)&format=json&nojsoncallback=1"
    print(url)
    return url
}


