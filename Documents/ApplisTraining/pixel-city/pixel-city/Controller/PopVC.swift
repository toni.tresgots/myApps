//
//  PopVC.swift
//  pixel-city
//
//  Created by Tony Tresgots on 05/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import Alamofire

class PopVC: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var popImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var passedImage: UIImage!       // le ! car forcément il a une image selectionné
    
    func initData(forImage image: UIImage) {           // open l'image choisis from the collectionView
        self.passedImage = image
    }
    
    func addDoubleTap() {
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(screenWasDoubleTapped))
        doubleTap.numberOfTapsRequired = 2
        doubleTap.delegate = self
        view.addGestureRecognizer(doubleTap)
    }
    
    @objc func screenWasDoubleTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popImageView.image = passedImage
        addDoubleTap()
    }


}
