//
//  ViewController.swift
//  vision-app-dev
//
//  Created by Tony Tresgots on 24/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML
import Vision

enum FlashState {
    case on, off
}

class CameraVC: UIViewController {
    
    var captureSession: AVCaptureSession!       // control the real time capture camera
    var cameraOutput: AVCapturePhotoOutput!     // capture photo from the captureSession
    var previewLayer: AVCaptureVideoPreviewLayer!   // added to backgroundView to show camera
    
    var photoData: Data?
    var flashControlState: FlashState = .off
    
    var speechSynthesizer = AVSpeechSynthesizer()
    
    
    @IBOutlet weak var nameOfItemLbl: UILabel!
    @IBOutlet weak var confidencePercLbl: UILabel!
    @IBOutlet weak var imageView: RoundedShadowImageView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var flashLbl: RoundedShadowButton!
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var roundedLblView: RoundedShadowView!
    
    
    @IBAction func flashBtnPressed(_ sender: UIButton) {
        switch flashControlState {
        case .off:
            flashLbl.setTitle("FLASH ON", for: .normal)
            flashControlState = .on
        case .on:
            flashLbl.setTitle("FLASH OFF", for: .normal)
            flashControlState = .off
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer.frame = cameraView.bounds      // le previewLayer fits the cameraView
        
        speechSynthesizer.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapCameraView))
        tap.numberOfTapsRequired = 1    // tap the screen and call didTapCameraView to take a photo
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.hd1920x1080  // capture full screen size
        
        let backCamera = AVCaptureDevice.default(for: AVMediaType.video)    // permet de capture full screen
        
        do {        // do try catch because forte possibilité d'error
            let input = try AVCaptureDeviceInput(device: backCamera!)   // input --> is the camera
            if captureSession.canAddInput(input) == true {           // simulateur n'a pas de caméra / check if there is a camera
                captureSession.addInput(input)      // need to start recording camera
            }
            
            cameraOutput = AVCapturePhotoOutput()       // output --> will be a photo
            
            if captureSession.canAddOutput(cameraOutput) == true {
                captureSession.addOutput(cameraOutput!)
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)            //instanciated with captureSession / need to display the camera
                previewLayer.videoGravity = AVLayerVideoGravity.resizeAspect    // garde le contenu vidéo en bonne forme
                previewLayer.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                
                cameraView.layer.addSublayer(previewLayer!)     // add to the cameraView
                cameraView.addGestureRecognizer(tap)
                captureSession.startRunning()
            }
            
        } catch {
            debugPrint(error)
        }
    }
    
    @objc func didTapCameraView() {
        self.cameraView.isUserInteractionEnabled = false    // avoid user to interact when we take a screenshot ( eviter qu'il click partout )
        self.spinner.isHidden = false
        self.spinner.startAnimating()
        
        let settings = AVCapturePhotoSettings()
        
        settings.previewPhotoFormat = settings.embeddedThumbnailPhotoFormat
        
        if flashControlState == .off {
            settings.flashMode = .off
        } else {
            settings.flashMode = .on
        }
        
        cameraOutput.capturePhoto(with: settings, delegate: self)
    }
    
    func resultMethod(request: VNRequest, error: Error?) {  // retrieve the one result with the best confidence
        guard let results = request.results as? [VNClassificationObservation] else { return }
        
        for classification in results {
            if classification.confidence < 0.5 {    // si la confidence est inferieur a 50% --> not sure
                let unknownItemMessage = "I'm not sure what it is, please try again"
                self.nameOfItemLbl.text = unknownItemMessage
                synthesizeSpeech(fromString: unknownItemMessage)
                self.confidencePercLbl.text = ""
                break   // leave the for loop because no need to continue
            } else {
                let identification = classification.identifier
                let confidence = Int(classification.confidence * 100)
                self.nameOfItemLbl.text = identification     // display the name of the item
                self.confidencePercLbl.text = "CONFIDENCE: \(confidence)%"
                let completeSentence = "This item is a \(identification), and i'm \(confidence) percent sure."
                synthesizeSpeech(fromString: completeSentence)  // it will speak the sentence
                break
            }
        }
    }
    
    func synthesizeSpeech(fromString string: String) {      // fonction enonciation de string / tout string entrant sera dit
        let speechUtterance = AVSpeechUtterance(string: string)
        speechSynthesizer.speak(speechUtterance)
    }

}

extension CameraVC: AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        if let error = error {
            debugPrint(error)
        } else {
            photoData = photo.fileDataRepresentation()  // le output --> photo, converti en photoData
            
            do {    // pass the photoData into the ML model
                let model = try VNCoreMLModel(for: SqueezeNet().model)  // SqueezeNet --> fichier SqueezeNet.mlmodel / .model --> var inside that fichier utilisable    /   model --> the brain
                let request = VNCoreMLRequest(model: model, completionHandler: resultMethod)     // resultMethod = func() but no need to display parameters because VNRequest has automatically a request and an error      /   request --> use the brain to make a reflexion
                let handler = VNImageRequestHandler(data: photoData!)      // handler will take the request ( reflexion ) and turn it with something we can use  /   ici handler analyze the photoData  /   need the handler to perform our request
                try handler.perform([request])
            } catch {
                debugPrint(error)
            }
            
            let image = UIImage(data: photoData!)   // on reconverti le output photoData en UIImage
        }
    }
}


extension CameraVC: AVSpeechSynthesizerDelegate {
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer, didFinish utterance: AVSpeechUtterance) {
        // une fois que le speech est fini ...
        self.cameraView.isUserInteractionEnabled = true     // le fait d'empecher le user d'interact when loading avoid too much inputs and avoid crashes
        self.spinner.isHidden = true
        self.spinner.stopAnimating()
    }
}







