//
//  RoundedShadowView.swift
//  vision-app-dev
//
//  Created by Tony Tresgots on 25/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

class RoundedShadowView: UIView {

    override func awakeFromNib() {
        self.layer.shadowColor = #colorLiteral(red: 0.2457633913, green: 0.2547563314, blue: 0.2807479501, alpha: 1)
        self.layer.shadowRadius = 15
        self.layer.shadowOpacity = 0.75
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    
}
