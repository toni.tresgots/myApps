//
//  GoalType.swift
//  goalpost-app
//
//  Created by Tony Tresgots on 10/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import Foundation

enum GoalType : String {
    case longTerm = "Long Term"
    case shortTerm = "Short Term"
}
