//
//  FinishGoalVC.swift
//  goalpost-app
//
//  Created by Tony Tresgots on 12/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import CoreData

class FinishGoalVC: UIViewController {

    @IBOutlet weak var createGoalBtn: UIButton!
    @IBOutlet weak var pointsTextField: UITextField!
    
    var goalDesc : String!
    var goalType : GoalType!
    
    @IBAction func createGoalPressed(_ sender: UIButton) {      // Pass data into Core Data Model   / Where we gonna save
        if pointsTextField.text != "" {
            self.save { (complete) in
                if complete {
                    dismiss(animated: true, completion: nil)
                }
            }
        }
        
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        dismissDetail()
    }
    
    func initData(description: String, type: GoalType) {    // init data to core data
        self.goalDesc = description
        self.goalType = type
    }
    
    func save(completion: (_ finished: Bool) -> ()) {       // SAVE IN CORE DATA
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }     // si nous ne pouvons pas y accéder on ne dois pas pouvoir aller plus loin donc --> return
        let goal = Goal(context: managedContext)        // instance entity model Core Data      // le managedContext = where the data will be saved dans le persistant contain
        
        goal.goalDescription = goalDesc     // goal.goalDescription --> propriété ajoutée dans le model entity, récupérable grace a l'instance goal
        goal.goalType = goalType.rawValue // CoreData accepte seulement les types général ( rawValue --> String pure )
        goal.goalCompletionValue = Int32(pointsTextField.text!)!    // .text --> String, ici un Int est nécessaire
        goal.goalProgress = Int32(0)
        
        do {
            try managedContext.save()   // func save() --> proviens du app delegate
            print("Successfully saved Data")
            completion(true)
        } catch {
            debugPrint("Could not safe: \(error.localizedDescription)")
            completion(false)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createGoalBtn.bindToKeyBoard()     // func que nous avons créee / bouton au dessus du clavier
    }
    
    
}
