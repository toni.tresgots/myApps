//
//  ViewController.swift
//  goalpost-app
//
//  Created by Tony Tresgots on 10/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as? AppDelegate     // instance AppDelegate pour utiliser les propriétés et certaine func deja données      // constante générale accessible dans tout les VC

class GoalVC: UIViewController {

    var goals: [Goal] = []      // tableau qui va stocker nos goals
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBAction func addGoalButtonPressed(_ sender: UIButton) {
        guard let createGoalVC = storyboard?.instantiateViewController(withIdentifier: "CreateGoalVC") else { return }
        presentDetail(createGoalVC)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchCoreDataObjets()
        tableView.reloadData()

    }
    
    func fetchCoreDataObjets() {
        self.fetch { (complete) in
            if complete {
                if goals.count >= 1 {           // si il y a au moins un goal
                    tableView.isHidden = false      // show tableView
                } else {
                    tableView.isHidden = true
                }
            }
        }
        
    }



}

extension GoalVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return goals.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "goalCell") as? GoalCell else { return UITableViewCell()}
        let goal = goals[indexPath.row]     // instance pour pouvoir utiliser les propriété dans le cellConfigure
        
        cell.configureCell(goal: goal)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true         // enable editing table view rows ( pour le swipe suppr. par ex )
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .none            // style du dash pour suppr. ici none car nous avons custom
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {     // edit the dash
        let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE") { (rowAction, indexPath) in
            self.removeGoal(atIndexPath: indexPath)
            self.fetchCoreDataObjets()
            tableView.deleteRows(at: [indexPath], with: .automatic)
        }
        
        let addAction = UITableViewRowAction(style: .normal, title: "ADD") { (rowAction, indexPath) in
            self.setProgress(atIndexPath: indexPath)
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
        
        deleteAction.backgroundColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        addAction.backgroundColor = #colorLiteral(red: 0.9607843161, green: 0.7058823705, blue: 0.200000003, alpha: 1)
        
        return [deleteAction, addAction]
    }
    
}


extension GoalVC {
    
    func setProgress(atIndexPath indexPath: IndexPath) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let chosenGoal = goals[indexPath.row]
        
        if chosenGoal.goalProgress < chosenGoal.goalCompletionValue {
            chosenGoal.goalProgress = chosenGoal.goalProgress + 1
        } else {
            return
        }
        
        do {
            try managedContext.save()
        } catch {
            debugPrint(error.localizedDescription)
        }
    }
    
    func removeGoal(atIndexPath indexPath: IndexPath) {     // remove from Core Data
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        managedContext.delete(goals[indexPath.row])
        
        do {
            try managedContext.save()
            print("Succesfully removed")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func fetch(completion: (_ complete: Bool) -> () ) {     // (_ complete: Bool) -> () signifie qu'il prend en entrée une fonction qui return quelque chose
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }            // get the data from the manager context
        
        let fetchRequest = NSFetchRequest<Goal>(entityName: "Goal")       // request for fetching data from the Goal entity
        
        do {
            goals = try managedContext.fetch(fetchRequest)      // retourne un array de data que l'on dois stocker dans une var
            print("Success fetch data")
            completion(true)
        } catch {
            debugPrint(error.localizedDescription)
            completion(false)
            print("Echec fetch data")
        }
        
    }
}









