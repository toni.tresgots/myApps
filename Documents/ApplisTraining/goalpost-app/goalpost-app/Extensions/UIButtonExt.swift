//
//  UIButtonExt.swift
//  goalpost-app
//
//  Created by Tony Tresgots on 11/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

extension UIButton {
    
    func setSelectedColor() {
        self.backgroundColor = #colorLiteral(red: 0.3568627451, green: 0.6235294118, blue: 0.7960784314, alpha: 1)
    }
    
    func setDeselectedColor() {
        self.backgroundColor = #colorLiteral(red: 0.006572759245, green: 0.3244492412, blue: 0.9899923205, alpha: 0.368953339)
    }
}
