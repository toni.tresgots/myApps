//
//  UIViewControllerExt.swift
//  goalpost-app
//
//  Created by Tony Tresgots on 11/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit

extension UIViewController {       // fonction disponible directement everywhere // custom animations
    
    func presentDetail(_ viewControllerToPresent : UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush     // type de transition --> ici push
        transition.subtype = kCATransitionFromRight     // d'ou sors la transition --> ici from right
        self.view.window?.layer.add(transition, forKey: kCATransition)      // add the transition
        // CATransition always has a key dedicated --> KCATransition
        
        present(viewControllerToPresent, animated: false, completion: nil)    // false car custom animation / si true animation par défaut
    }
    
    func presentSecondaryDetail(_ viewControllerToPresent : UIViewController) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        
        guard let presentedViewController = presentedViewController else { return }
        
        presentedViewController.dismiss(animated: false) {
            self.view.window?.layer.add(transition, forKey: kCATransition)
            self.present(viewControllerToPresent, animated: false, completion: nil)
        }
    }
    
    func dismissDetail() {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft     // d'ou fini la transition --> ici from right
        self.view.window?.layer.add(transition, forKey: kCATransition)
        
        dismiss(animated: false, completion: nil)
    }
}
