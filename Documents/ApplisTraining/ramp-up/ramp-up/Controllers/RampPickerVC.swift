//
//  RampPickerVC.swift
//  ramp-up
//
//  Created by Tony Tresgots on 01/03/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import SceneKit

class RampPickerVC: UIViewController {      // --> le popover VC ou l'on choisis nos items
    
    var sceneView: SCNView!
    var size: CGSize!
    weak var rampPlacerVC = RampPlacerVC()
    
    init(size: CGSize) {    // need a popover to present the models - first we need a size
        super.init(nibName: nil, bundle: nil)
        self.size = size
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented") // en cas d'erreur / not important but required
    }
    
    @objc func handleTap(_ gestureRecognizer: UIGestureRecognizer) {    // when screen is tapped
        let p = gestureRecognizer.location(in: sceneView)       // detect the location ( the point ) of the tap on the screen view
        let hitResults = sceneView.hitTest(p, options: [:])    // detect if an object is hit/tapped inside the sceneView
        
        if hitResults.count > 0 {   // if we tap on an object
            let node = hitResults[0].node   // grab the node ( object ) hitted
            print(node.name!)   // le nom de l'objet apparait dans la console
            rampPlacerVC?.onRampSelected(node.name!)    // retrieve the object tapped name
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.frame = CGRect(origin: CGPoint.zero, size: size)
        sceneView = SCNView(frame: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        view.insertSubview(sceneView, at: 0)        // size definie
        
        view.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        view.layer.borderWidth = 3.0    // border du popover
        
        let scene = SCNScene(named: "art.scnassets/ramps.scn")! // on recup notre scene vide
        sceneView.scene = scene     // la scene ramps.scn qui sera displayed in the SceneView
        // pour changer le background --> dans l'interface builder / pour image --> mettre le file dans les textures
        
        let camera = SCNCamera()     // pour changer les ramps du popover rampPicker
        camera.usesOrthographicProjection = true    // enleve la profondeur de la 3D / plus adapté
        scene.rootNode.camera = camera  // modifie le type de camera en OrthoProj
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))    // when item is tapped

        let pipe = Ramp.getPipes()
        Ramp.startRotation(node: pipe)      // refactored with Ramp class
        scene.rootNode.addChildNode(pipe)
        
        let pyramid = Ramp.getPyramid()
        Ramp.startRotation(node: pyramid)
        scene.rootNode.addChildNode(pyramid)
        
        let quarter = Ramp.getQuarter()
        Ramp.startRotation(node: quarter)
        scene.rootNode.addChildNode(quarter)
        
        preferredContentSize = size

        }

}
