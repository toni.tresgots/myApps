//
//  ViewController.swift
//  ramp-up
//
//  Created by Tony Tresgots on 28/02/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class RampPlacerVC: UIViewController, ARSCNViewDelegate, UIPopoverPresentationControllerDelegate {
    // UIPopoverDelegate --> style de la présentation du popover RampPicker
    
    var selectedRampName : String?
    var selectedRamp : SCNNode?

    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var controls: UIStackView!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var rotateBtn: UIButton!
    @IBOutlet weak var upBtn: UIButton!
    @IBOutlet weak var downBtn: UIButton!
    
    
    
    @IBAction func onRemovePressed(_ sender: UIButton) {
        if let ramp = selectedRamp {        // if there is a selected ramp
            ramp.removeFromParentNode()
            selectedRamp = nil  // after removed = nil
            
        }
    }
    
    @IBAction func onRotatePressed(_ sender: UIButton) {
        
    }
    
    
    
    @IBAction func onRampBtnPressed(_ sender: UIButton) {
        let rampPickerVC = RampPickerVC(size: CGSize(width: 250, height: 500))
        rampPickerVC.rampPlacerVC = self    // send the rampPickerVC to the rampPlacerVC
        rampPickerVC.modalPresentationStyle = .popover
        rampPickerVC.popoverPresentationController?.delegate = self
        
        present(rampPickerVC, animated: true, completion: nil)
        rampPickerVC.popoverPresentationController?.sourceView = sender
        rampPickerVC.popoverPresentationController?.sourceRect = sender.bounds
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none        // si on oublie cette func --> popover full screen
    }
    
    func onRampSelected(_ rampName : String) {
        selectedRampName = rampName
    }
    
    func placeRamp(position: SCNVector3) {
        if let rampName = selectedRampName {
            controls.isHidden = false
            let ramp = Ramp.getRampName(rampName: rampName)
            selectedRamp = ramp
            ramp.position = position        // position of the 3D object in the camera
            ramp.scale = SCNVector3Make(0.01, 0.01, 0.01)   // taille par defaut de l'objet
            sceneView.scene.rootNode.addChildNode(ramp)
        }
    }
    
    @objc func onLongPress(gesture: UILongPressGestureRecognizer) {
        if let ramp = selectedRamp {    // si une ramp est selected
            if gesture.state == .ended {        // if rotateBtn no more pressed --> remove all actions ( no more rotation )
                ramp.removeAllActions()
            } else if gesture.state == .began {     // gesture.state --> pressed or not
                if gesture.view === rotateBtn {     // if the view pressed is the rotateBtn
                    let rotate = SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: CGFloat(0.08 * Double.pi), z: 0, duration: 0.1))
                    ramp.runAction(rotate)      // when button rotateBtn pressed rotate forever until unpressed
                } else if gesture.view === upBtn {
                    let moveUp = SCNAction.repeatForever(SCNAction.moveBy(x: 0, y: 0.08, z: 0, duration: 0.1))  // move up
                    ramp.runAction(moveUp)
                } else if gesture.view === downBtn {
                    let moveDown = SCNAction.repeatForever(SCNAction.moveBy(x: 0, y: -0.08, z: 0, duration: 0.1))  // move down
                    ramp.runAction(moveDown)
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        
        // Create a new scene   /   sinon pas de AR inside the camera
        let scene = SCNScene(named: "art.scnassets/main.scn")!
        sceneView.autoenablesDefaultLighting = true
        
        // Set the scene to the view
        sceneView.scene = scene
        
        let gesture1 = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(gesture:)))
        let gesture2 = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(gesture:)))
        let gesture3 = UILongPressGestureRecognizer(target: self, action: #selector(onLongPress(gesture:)))
        gesture1.minimumPressDuration = 0.1
        gesture2.minimumPressDuration = 0.1
        gesture3.minimumPressDuration = 0.1
        rotateBtn.addGestureRecognizer(gesture1)
        upBtn.addGestureRecognizer(gesture2)
        downBtn.addGestureRecognizer(gesture3)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingConfiguration()

        // Run the view's session
        sceneView.session.run(configuration)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {  // detecte the touch to place the object / common process for ARKit
        guard let touch = touches.first else { return }     // grab the touch
        let results = sceneView.hitTest(touch.location(in: sceneView), types: [.featurePoint])  // .featurePoint --> ARKit property ( detecte murs, sols etc )
        
        guard let hitFeature = results.last else { return } // releve les murs/sols, si jamais trop sombres/impossible de placer object --> return empty
        let hitTransform = SCNMatrix4(hitFeature.worldTransform)    // but if we got something --> transform it for AR to place 3D obj
        let hitPosition = SCNVector3Make(hitTransform.m41, hitTransform.m42, hitTransform.m43)
        placeRamp(position: hitPosition)
    }
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
