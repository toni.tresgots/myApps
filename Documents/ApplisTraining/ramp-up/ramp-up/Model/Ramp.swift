//
//  Ramp.swift
//  ramp-up
//
//  Created by Tony Tresgots on 03/03/2018.
//  Copyright © 2018 Tony Tresgots. All rights reserved.
//

import Foundation
import SceneKit

class Ramp {        // si nous voulons simplifier -> funcs for refactoring and reuse / je ne les ai pas utilisées
    
    class func getRampName(rampName: String) -> SCNNode {
        switch rampName {
        case "pipe":
            return Ramp.getPipes()
        case "pyramid":
            return Ramp.getPyramid()
        case "quarter":
            return Ramp.getQuarter()
        default:
            return Ramp.getPipes()
        }
    }
    
    class func getPipes() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/pipe.dae")!    // on recupere la scene entiere pipe.dae
        let node = obj.rootNode.childNode(withName: "pipe", recursively: true)  // on recupere seulement l'objet de la scene pipe.dae / "pipe" étant l'id donné a l'objet
        node?.scale = SCNVector3Make(0.0022, 0.0022, 0.0022)   // modifie taille / diminution taille
        node?.position = SCNVector3Make(-1, 0.7, -1)  // modifie position
        return node!
    }
    
    class func getPyramid() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/pyramid.dae")!
        let node = obj.rootNode.childNode(withName: "pyramid", recursively: true)
        node?.scale = SCNVector3Make(0.0058, 0.0058, 0.0058)
        node?.position = SCNVector3Make(-1, -0.62, -1)
        return node!
    }
    
    class func getQuarter() -> SCNNode {
        let obj = SCNScene(named: "art.scnassets/quarter.dae")!
        let node = obj.rootNode.childNode(withName: "quarter", recursively: true)
        node?.scale = SCNVector3Make(0.0058, 0.0058, 0.0058)
        node?.position = SCNVector3Make(-1, -2.4, -1)
        return node!
    }
    
    class func startRotation(node: SCNNode) {
        let rotate = SCNAction.repeatForever(SCNAction.rotateBy(x: 0, y: CGFloat(0.01 * Double.pi), z: 0, duration: 0.1))   // rotation des items du popover
        node.runAction(rotate)     // rotate item
    }
}








